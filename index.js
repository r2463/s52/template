function countLetter(letter, sentence) {
    let result = 0;


    // Check first whether the letter is a single character.


    //Conditons:
        // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
        // If letter is invalid, return undefined.


    if (letter.length == 1){

        for (i = 0; i < sentence.length; i++){

            if(sentence[i] == letter){
                result++;

            };

            };
        
        return result;   
    }   
 }



function isIsogram(text) {
    //Goal:
        // An isogram is a word where there are no repeating letters.

    //Check:
        // The function should disregard text casing before doing anything else.


    //Condition:
        // If the function finds a repeating letter, return false. Otherwise, return true.


        return !text.match(/([a-z]).*\1/i);
}

function purchase(age, price) {

    //Conditions:
        // Return undefined for people aged below 13.
        // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
        // Return the rounded off price for people aged 22 to 64.

    //Check:
        // The returned value should be a string.
    

    if (age >= 13 && age <= 21){

      price = (price * 0.8).toFixed(2)
      return price
    
    }
    else if (age >=65){

        price = (price * 0.8).toFixed(2)
        return price
        
    }
    else if (age >= 22 && age <= 64){

        price = (price).toFixed(2)
        return price
    }


}

function findHotCategories(items) {
    //Goal:
        // Find categories that has no more stocks.
        // The hot categories must be unique; no repeating categories.

    //Array for the test:
        // The passed items array from the test are the following:
        // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
        // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
        // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
        // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
        // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }


    //Expected return must be array:
        // The expected output after processing the items array is ['toiletries', 'gadgets'].

    // Note:
        // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

        let result = [];    
        
        for(let i = 0; i < items.length; i++){
          if(items[i].stocks === 0){
            result.push(items[i].category)

          }
       }
       return [...new Set(result)];
}

function findFlyingVoters(candidateA, candidateB) {
    //Goal:
        // Find voters who voted for both candidate A and candidate B.

    //Array for the test:    
        // The passed values from the test are the following:
        // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
        // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // Expected return must be array:
        // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].

    //Note:
        // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

        let result = [];

        for ( i = 0; i < candidateA.length; i++ ) {
            for ( e = 0; e < candidateB.length; e++ ) {
                if ( candidateA[i] === candidateB[e] ) result.push( candidateA[i] );
            }
        }
        return result
}


    


module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};